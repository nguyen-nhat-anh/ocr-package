from setuptools import setup

setup(
    name="ocr",
    version="0.1.0",
    packages=["ocr"],
    install_requires=[
        "torch",
        "torchvision",
        "onnx",
        "numpy",
        "pyyaml",
    ],
    extras_require={
        "cpu": ["paddlepaddle", "onnxruntime"],
        "gpu": ["paddlepaddle-gpu", "onnxruntime-gpu"]
    }
)
