import cv2
import torch
import onnxruntime
import numpy as np
import torch.nn.functional as F
from PIL.Image import Image
from typing import List, Tuple, Union
from collections import defaultdict
from vietocr.tool.config import Cfg
from vietocr.tool.translate import process_input
from vietocr.model.vocab import Vocab
from paddleocr import PaddleOCR


class TextDetectionModel:
    def __init__(self):
        """Text detection using PaddleOCR"""
        self.model = PaddleOCR(
            det_fce_box_type="rectangular",
            use_angle_cls=False,
            lang="en",
            show_log=False,
            use_gpu=True
        )

    def predict(self, img: Image, textbox_margin: int) -> List:
        """Detect textline in image

        Args:
            img (Image): input image
            textbox_margin (int): textbox margin to expand

        Returns:
            List: list of bounding box, each bounding box is a list [xmin, ymin, xmax, ymax]
        """
        w, h = img.size
        img_bgr = cv2.cvtColor(np.asarray(img), cv2.COLOR_RGB2BGR)
        # paddle ocr receives bgr image as input
        result = self.model.ocr(img_bgr, cls=False, rec=False)
        textboxes = []
        for r in result:
            xmin = max(min([p[0] for p in r]) - textbox_margin, 0)
            xmax = min(max([p[0] for p in r]) + textbox_margin, w)
            ymin = max(min([p[1] for p in r]) - textbox_margin, 0)
            ymax = min(max([p[1] for p in r]) + textbox_margin, h)
            textboxes.append([xmin, ymin, xmax, ymax])
        return textboxes


class TextRecognitionModel:
    def __init__(
        self, cnn_path: str, encoder_path: str, decoder_path: str, config: Cfg
    ):
        """Text recognition using vietocr vgg-transformer/vgg-seq2seq

        Args:
            cnn_path (str): path to onnx exported cnn backbone model
            encoder_path (str): path to onnx exported encoder model
            decoder_path (str): path to onnx exported decoder model
            config (Cfg): model config
        """
        self.cnn_session = onnxruntime.InferenceSession(cnn_path)
        self.encoder_session = onnxruntime.InferenceSession(encoder_path)
        self.decoder_session = onnxruntime.InferenceSession(decoder_path)
        self.config = config
        self.vocab = Vocab(config["vocab"])

    def translate(
        self,
        img: torch.Tensor,
        max_seq_length: int = 128,
        sos_token: int = 1,
        eos_token: int = 2,
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Translate textline image (torch tensor) to string

        Args:
            img (torch.Tensor): image tensor, shape (1, 3, h, w)
            max_seq_length (int, optional): max sequence length. Defaults to 128.
            sos_token (int, optional): start of string token. Defaults to 1.
            eos_token (int, optional): end of string token. Defaults to 2.

        Returns:
            Tuple[np.ndarray, np.ndarray]: translated_sentence, char_probs
             translated_sentence (numpy.ndarray): array of int (encoded char), shape (1, n)
             char_probs (numpy.ndarray): shape (1, )
        """
        src = self.cnn_session.run(None, {"img": img.numpy()})
        src = src[0]
        encoder_outputs, hidden = self.encoder_session.run(None, {"src": src})

        translated_sentence = [[sos_token] * len(img)]
        char_probs = [[1] * len(img)]

        max_length = 0

        while max_length <= max_seq_length and not all(
            np.any(np.asarray(translated_sentence).T == eos_token, axis=1)
        ):
            tgt_inp = np.array(translated_sentence[-1])
            output, hidden, attention = self.decoder_session.run(
                None,
                {
                    "tgt": tgt_inp,
                    "hidden_in": hidden,
                    "encoder_outputs": encoder_outputs,
                },
            )
            output = torch.from_numpy(output).unsqueeze(1)
            output = F.softmax(output, dim=-1)
            values, indices = torch.topk(output, 5)

            indices = indices[:, -1, 0]
            indices = indices.tolist()

            values = values[:, -1, 0]
            values = values.tolist()
            char_probs.append(values)

            translated_sentence.append(indices)
            max_length += 1

            del output

        translated_sentence = np.asarray(translated_sentence).T

        char_probs = np.asarray(char_probs).T
        char_probs = np.multiply(char_probs, translated_sentence > 3)
        char_probs = np.sum(char_probs, axis=-1) / (char_probs > 0).sum(-1)
        return translated_sentence, char_probs

    def predict(
        self, img: Image, return_prob: bool = False
    ) -> Union[Tuple[str, float], str]:
        """Textline image recognition (image to string)

        Args:
            img (Image): textline image
            return_prob (bool, optional): whether to return probability or not. Defaults to False.

        Returns:
            Union[Tuple[str, float], str]: (s, prob) or s
             s: output string
             prob: probability
        """
        img = process_input(
            img,
            self.config["dataset"]["image_height"],
            self.config["dataset"]["image_min_width"],
            self.config["dataset"]["image_max_width"],
        )
        s, prob = self.translate(img)
        s = s[0].tolist()
        prob = prob[0]

        s = self.vocab.decode(s)

        if return_prob:
            return s, prob
        else:
            return s

    def predict_batch(
        self, imgs: List[Image], return_prob: bool = False
    ) -> Union[Tuple[List[str], List[float]], List[str]]:
        """Textline image recognition (image to string) batch version

        Args:
            imgs (List[Image]): textline images
            return_prob (bool, optional): whether to return probability or not. Defaults to False.

        Returns:
            Union[Tuple[List[str], List[float]], List[str]]: (sents, probs) or sents
             sents: output strings
             probs: probabilities
        """
        bucket = defaultdict(list)
        bucket_idx = defaultdict(list)
        bucket_pred = {}

        sents, probs = [0] * len(imgs), [0] * len(imgs)

        for i, img in enumerate(imgs):
            img = process_input(
                img,
                self.config["dataset"]["image_height"],
                self.config["dataset"]["image_min_width"],
                self.config["dataset"]["image_max_width"],
            )

            bucket[img.shape[-1]].append(img)
            bucket_idx[img.shape[-1]].append(i)

        for k, batch in bucket.items():
            batch = torch.cat(batch, 0)
            s, prob = self.translate(batch)
            prob = prob.tolist()

            s = s.tolist()
            s = self.vocab.batch_decode(s)

            bucket_pred[k] = (s, prob)

        for k in bucket_pred:
            idx = bucket_idx[k]
            sent, prob = bucket_pred[k]
            for i, j in enumerate(idx):
                sents[j] = sent[i]
                probs[j] = prob[i]

        if return_prob:
            return sents, probs
        else:
            return sents


class OcrResult:
    def __init__(self, bbox, text):
        self._bbox = bbox
        self._text = text

    def __repr__(self) -> str:
        return f"<{self.bbox} '{self.text}'>"

    @property
    def bbox(self):
        return self._bbox

    @bbox.setter
    def bbox(self, value):
        self._bbox = value

    @property
    def text(self):
        return self._text

    def get_text(self):
        return self.text


class OcrModel:
    def __init__(
        self,
        text_detection_model: TextDetectionModel,
        text_recognition_model: TextRecognitionModel,
    ):
        """OCR pipeline (text recognition + text recognition)

        Args:
            text_detection_model (TextDetectionModel): text detection model
            text_recognition_model (TextRecognitionModel): text recognition model
        """
        self.td_model = text_detection_model
        self.tr_model = text_recognition_model

    def predict(self, img: Image, textbox_margin: int = 7) -> List[OcrResult]:
        """textline ocr from image

        Args:
            img (Image): input image
            textbox_margin (int, optional): textbox margin to expand. Defaults to 7.

        Returns:
            List[OcrResult]: ocr results
        """
        textboxes = self.td_model.predict(img, textbox_margin)
        textline_imgs = []
        for textbox in textboxes:
            textline_img = img.crop(box=textbox)
            textline_imgs.append(textline_img)
        texts = self.tr_model.predict_batch(textline_imgs)
        ocr_results = [OcrResult(bbox, text) for bbox, text in zip(textboxes, texts)]
        ocr_results = sorted(ocr_results, key=lambda x: (x.bbox[1], x.bbox[0]))
        return ocr_results
