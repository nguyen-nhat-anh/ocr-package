import torch
from pathlib import Path
from vietocr.tool.translate import build_model
from vietocr.tool.utils import download_weights
from vietocr.tool.config import Cfg


def export_seq2seq(model_dir: str, opset_version: int = 12) -> None:
    """export vgg-seq2seq model

    Args:
        model_dir (str): path to output model directory
        opset_version (int, optional): onnx opset version. Defaults to 12.
    """
    config = Cfg.load_config_from_name("vgg_seq2seq")
    config["cnn"]["pretrained"] = False
    config["device"] = "cpu"
    config["predictor"]["beamsearch"] = False

    model, vocab = build_model(config)
    weights = download_weights(config["weights"])
    model.load_state_dict(
        torch.load(weights, map_location=torch.device(config["device"]))
    )
    model.eval()

    img = torch.rand(1, 3, 32, 512).to(config["device"])
    with torch.no_grad():
        src = model.cnn(img)
        encoder_outputs, hidden_in = model.transformer.encoder(src)
        tgt = torch.LongTensor([1] * len(img)).to(config["device"])
        output, hidden_out, attention = model.transformer.decoder(
            tgt, hidden_in, encoder_outputs
        )

    Path(model_dir).mkdir(parents=True, exist_ok=True)
    torch.onnx.export(
        model.cnn,
        img,
        str(Path(model_dir) / f"vgg_seq2seq_cnn.onnx"),
        export_params=True,
        opset_version=opset_version,
        do_constant_folding=True,
        input_names=["img"],
        output_names=["src"],
        dynamic_axes={
            "img": {0: "batch_size", 3: "img_width"},
            "src": {0: "src_len", 1: "batch_size"},
        },
    )
    torch.onnx.export(
        model.transformer.encoder,
        src,
        str(Path(model_dir) / f"vgg_seq2seq_encoder.onnx"),
        export_params=True,
        opset_version=opset_version,
        do_constant_folding=True,
        input_names=["src"],
        output_names=["encoder_outputs", "hidden"],
        dynamic_axes={
            "src": {0: "src_len", 1: "batch_size"},
            "encoder_outputs": {0: "src_len", 1: "batch_size"},
            "hidden": {0: "batch_size"},
        },
    )
    torch.onnx.export(
        model.transformer.decoder,
        (tgt, hidden_in, encoder_outputs),
        str(Path(model_dir) / f"vgg_seq2seq_decoder.onnx"),
        export_params=True,
        opset_version=opset_version,
        do_constant_folding=True,
        input_names=["tgt", "hidden_in", "encoder_outputs"],
        output_names=["output", "hidden_out", "attention"],
        dynamic_axes={
            "tgt": {0: "batch_size"},
            "hidden_in": {0: "batch_size"},
            "encoder_outputs": {0: "src_len", 1: "batch_size"},
            "output": {0: "batch_size"},
            "hidden_out": {0: "batch_size"},
            "attention": {0: "batch_size", 1: "src_len"},
        },
    )


# TODO: export transformer
# def export_transformer(model_dir, opset_version=12):
#     config = Cfg.load_config_from_name('vgg_transformer')
#     config['cnn']['pretrained']=False
#     config['device'] = 'cpu'
#     config['predictor']['beamsearch']=False

#     model, vocab = build_model(config)
#     weights = download_weights(config['weights'])
#     model.load_state_dict(torch.load(weights, map_location=torch.device(config['device'])))
#     model.eval()

#     img = torch.rand(1, 3, 32, 512).to(config['device'])
#     with torch.no_grad():
#         src = model.cnn(img)
